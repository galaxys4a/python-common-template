def say_hello(name: str | None = None) -> None:
    if name is not None:
        print(f"Hello, {name}!")
    else:
        print("Hello, World!")
