from hello import say_hello


def main() -> None:
    say_hello("Mike")


if __name__ == "__main__":
    main()
